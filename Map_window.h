//
// Created by kacper on 16.05.19.
//

#ifndef FLAPPY_BIRDO_MAP_WINDOW_H
#define FLAPPY_BIRDO_MAP_WINDOW_H
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics.hpp>



class Map_window{

public:
    sf::RectangleShape Tube_down1;
    sf::RectangleShape Tube_up1;
    sf::RectangleShape Tube_down2;
    sf::RectangleShape Tube_up2;
    sf::RectangleShape Tube_down3;
    sf::RectangleShape Tube_up3;


    Map_window();
    void draw ();
};







#endif //FLAPPY_BIRDO_MAP_WINDOW_H
