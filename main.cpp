#include <iostream>
#include <SFML/Graphics.hpp>

#include "Map_window.h"
#include "Bird.h"
#include "game.h"

#define Screen_width 1024
#define Screen_height 600

int main() {

    sf::RenderWindow window(sf::VideoMode(Screen_width, Screen_height), "Flappy Birdo");
    srand(time(NULL));

    Map_window map;
    map.draw();
    Bird birdo;
    birdo.Draw_bird();
    while (window.isOpen()) {
        sf::Event event;

        if (map.Tube_up1.getPosition().x <= 0 or map.Tube_down1.getPosition().x <= 0) {
            while (true) {
                int hight_of_tube_down = rand() % 450 + 150;
                int hight_of_tube_up = rand() % 300 - 301;

                if (Screen_height - abs(hight_of_tube_down - hight_of_tube_up) == 100) {
                    map.Tube_down1.setPosition(900, hight_of_tube_down);
                    map.Tube_up1.setPosition(900, hight_of_tube_up);
                    break;
                }

            }
        }

        if (map.Tube_up2.getPosition().x <= 0 or map.Tube_down2.getPosition().x <= 0) {
            while (true) {
                int hight_of_tube_down = rand() % 450 + 150;
                int hight_of_tube_up = rand() % 300 - 301;

                if (Screen_height - abs(hight_of_tube_down - hight_of_tube_up) == 100) {
                    map.Tube_down2.setPosition(900, hight_of_tube_down);
                    map.Tube_up2.setPosition(900, hight_of_tube_up);
                    break;
                }

            }
        }

        if (map.Tube_up3.getPosition().x <= 0 or map.Tube_down3.getPosition().x <= 0) {
            while (true) {
                int hight_of_tube_down = rand() % 450 + 150;
                int hight_of_tube_up = rand() % 300 - 301;

                if (Screen_height - abs(hight_of_tube_down - hight_of_tube_up) == 100) {
                    map.Tube_down3.setPosition(900, hight_of_tube_down);
                    map.Tube_up3.setPosition(900, hight_of_tube_up);
                    break;
                }

            }
        }

        if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::Space) {
                birdo.map_bird.move(0, -60);
            }
        }

        if (birdo.map_bird.getPosition().y <= Screen_height - 20) {


            if (birdo.map_bird.getPosition().y >= 578 or
                (map.Tube_up1.getPosition().x -  birdo.map_bird.getPosition().x < 10   and map.Tube_up1.getPosition().y -    birdo.map_bird.getPosition().y >10)
                or
                (map.Tube_down1.getPosition().x -  birdo.map_bird.getPosition().x <  10   and map.Tube_down1.getPosition().y -    birdo.map_bird.getPosition().y  <  10)
            or
            (map.Tube_down2.getPosition().x -  birdo.map_bird.getPosition().x <  10   and map.Tube_down2.getPosition().y -    birdo.map_bird.getPosition().y  <  10)
            or
            (map.Tube_down3.getPosition().x -  birdo.map_bird.getPosition().x <  10   and map.Tube_down3.getPosition().y -    birdo.map_bird.getPosition().y  <  10))
//                (abs(map.Tube_up1.getPosition().x - 20) == birdo.map_bird.getPosition().x  and
//                map.Tube_up1.getPosition().y <= birdo.map_bird.getPosition().y)
               // )
                {
                window.close();
            } else
                std::cout<< "tuba: " << (map.Tube_up1.getPosition().y ) << "ptaszor:" << birdo.map_bird.getPosition().y << std::endl;
                std:: cout << "warunek"  <<  (map.Tube_up1.getPosition().y < birdo.map_bird.getPosition().y) << std::endl;
                birdo.map_bird.move(0, 0.20);
            }


            if (map.Tube_up1.getPosition().x <= Screen_width - 20) {
                map.Tube_up1.move(-0.15, 0);
            }

            if (map.Tube_down1.getPosition().x <= Screen_width - 20) {
                map.Tube_down1.move(-0.15, 0);
            } else map.Tube_down1.setPosition(1024, 400);

            if (map.Tube_up2.getPosition().x <= Screen_width - 20) {
                map.Tube_up2.move(-0.15, 0);
            } else map.Tube_up2.setPosition(1024, 400);
            if (map.Tube_down2.getPosition().x <= Screen_width - 20) {
                map.Tube_down2.move(-0.15, 0);
            } else map.Tube_down2.setPosition(1024, 400);
            if (map.Tube_up3.getPosition().x <= Screen_width - 20) {
                map.Tube_up3.move(-0.15, 0);
            }
            if (map.Tube_down3.getPosition().x <= Screen_width - 20) {
                map.Tube_down3.move(-0.15, 0);
            }
            window.clear();
            window.draw(map.Tube_up1);
            window.draw(map.Tube_down1);
            window.draw(map.Tube_up2);
            window.draw(map.Tube_down2);
            window.draw(map.Tube_up3);
            window.draw(map.Tube_down3);
            window.draw(birdo.map_bird);
            window.display();


            if (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed)
                    window.close();
            }


        }


    }